'use strict';

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = chai.expect;
const userMapper = require('../../mappers/userMapper');

chai.use(chaiAsPromised);

describe('userMapper', () => {
  let testUsers;

  beforeEach(() => {
    testUsers = ['u1', 'u2'];
  });

  describe('buildPairingModel', () => {
    it('should return user data', () => {
      const result = userMapper.buildPairingModel(testUsers);
      expect(result.length).to.equal(2);
      expect(result).to.deep.include({ users: ['u1', 'u2'] });
      expect(result).to.deep.include({ users: ['u2', 'u1'] });
    });
  });

  describe('mapUserModels', () => {
    it('should map user view models', () => {
      const userModels = [{
        name: { first: 'James', last: 'Joyce' },
        email: 'test@mail.com',
        phone: '+665878787',
        guid: '3348-dj34jh4-c8c8uj3'
      }];

      expect(userMapper.mapUserModels(userModels)).to.deep.equal([{
        displayName: `${userModels[0].name.first} ${userModels[0].name.last}`,
        email: userModels[0].email,
        phone: userModels[0].phone,
        userId: userModels[0].guid
      }]);
    });
  });
});
