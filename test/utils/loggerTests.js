'use strict';

/* eslint-disable no-console */
const sinon = require('sinon');
const chai = require('chai');
const expect = chai.expect;
const proxyquire = require('proxyquire').noCallThru();

describe('logger', () => {
  let configMock;
  let logger;

  before(() => {
    sinon.spy(console, 'log');
  });

  beforeEach(() => {
    configMock = {
      logger: {
        groupName: 'TestGroup',
        serviceName: 'UI',
        environment: 'ACC'
      }
    };
    logger = proxyquire('../../utils/logger', {
      config: configMock
    });
  });

  it('should log decorated model on error', () => {
    const testMessage = 'service failure';
    const testData = { input: 884375 };
    logger.error(testMessage, testData);
    const logModel = console.log.lastCall.args[0];
    expect(logModel.message).to.equal(testMessage);
    expect(logModel.data).to.deep.equal(testData);
    expect(logModel.service).to.equal(configMock.logger.serviceName);
    expect(logModel.group).to.equal(configMock.logger.groupName);
    expect(logModel.environment).to.equal(configMock.environment);
  });

  it('should log decorated model on info', () => {
    const testMessage = 'something happened';
    const testData = { time: Date.now() };
    logger.info(testMessage, testData);
    const logModel = console.log.lastCall.args[0];
    expect(logModel.message).to.equal(testMessage);
    expect(logModel.data).to.deep.equal(testData);
    expect(logModel.service).to.equal(configMock.logger.serviceName);
    expect(logModel.group).to.equal(configMock.logger.groupName);
    expect(logModel.environment).to.equal(configMock.environment);
  });
});
