'use strict';

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = chai.expect;
const proxyquire = require('proxyquire').noCallThru();

chai.use(chaiAsPromised);

describe('userRepository', () => {
  let userRepository;
  let usersMock;

  beforeEach(() => {
    usersMock = { users: ['u1', 'u2'] };

    userRepository = proxyquire('../../repositories/userRepository', {
      '../data/users': usersMock
    });
  });

  describe('getUsers', () => {
    it('should return user data', () => {
      const result = userRepository.getUsers();
      expect(result).to.equal(usersMock.users);
    });
  });
});
