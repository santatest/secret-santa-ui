'use strict';

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = chai.expect;
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();

chai.use(chaiAsPromised);

describe('indexController', () => {
  let indexController;
  let userServiceMock;
  let userMapperMock;
  let testRequest;
  let testResponse;

  beforeEach(() => {
    // Mocks
    testRequest = {
      body: {},
      headers: {},
      params: {},
      query: {}
    };
    testResponse = {
      locals: {},
      render: sinon.stub()
    };
    userServiceMock = {
      getUsers: sinon.stub()
    };
    userMapperMock = {
      buildPairingModel: sinon.stub(),
      mapUserModels: sinon.stub()
    };

    // Setup
    indexController = proxyquire('../../controllers/indexController', {
      '../mappers/userMapper': userMapperMock,
      '../services/userService': userServiceMock
    });
  });

  describe('GET index', () => {
    it('should retrieve users from userService', () => {
      indexController.renderIndex(testRequest, testResponse);
      expect(userServiceMock.getUsers.called).to.be.true;
    });

    it('should map user models', () => {
      const userPairs = 'user pairs';
      userServiceMock.getUsers.returns(userPairs);
      indexController.renderIndex(testRequest, testResponse);
      expect(userMapperMock.mapUserModels.calledWith(userPairs)).to.be.true;
    });

    it('should build pairing model', () => {
      const mappedUserModels = ['user1', 'user2'];
      userMapperMock.mapUserModels.returns(mappedUserModels);
      indexController.renderIndex(testRequest, testResponse);
      expect(userMapperMock.buildPairingModel.calledWith(mappedUserModels)).to.be.true;
    });

    it('should render index view on success', () => {
      const testPairings = ['p1', 'p2'];
      userMapperMock.buildPairingModel.returns(testPairings);
      indexController.renderIndex(testRequest, testResponse);
      expect(testResponse.render.calledWith('index', { pairings: testPairings })).to.be.true;
    });
  });
});
