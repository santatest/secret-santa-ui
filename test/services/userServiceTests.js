'use strict';

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = chai.expect;
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();

chai.use(chaiAsPromised);

describe('userService', () => {
  let userService;
  let userRepositoryMock;

  beforeEach(() => {
    userRepositoryMock = {
      getUsers: sinon.stub()
    };

    userService = proxyquire('../../services/userService', {
      '../repositories/userRepository': userRepositoryMock
    });
  });

  describe('getUsers', () => {
    it('should passthrough call to userRepository.getUsers', () => {
      userService.getUsers();
      expect(userRepositoryMock.getUsers.called).to.be.true;
    });
  });
});
