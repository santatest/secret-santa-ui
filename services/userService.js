'use strict';

const userRepository = require('../repositories/userRepository');

module.exports = {
  getUsers
};

function getUsers() {
  return userRepository.getUsers();
}
