'use strict';

// Core Modules
const favicon = require('serve-favicon');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const path = require('path');
const config = require('config');
const swig = require('swig');
const morgan = require('morgan');
// i18n modules
const i18next = require('i18next');
const i18nBackend = require('i18next-node-fs-backend');
const i18nExpress = require('i18next-express-middleware');

module.exports = function (express, app) {
  // Core Modules
  app.set('views', path.join(__dirname, '../views'));
  app.engine('html', swig.renderFile);
  app.set('view engine', 'html');
  swig.setDefaults({
    varControls: ['[[', ']]']
  });

  if (config.baseUrl) {
    app.use(config.baseUrl, express.static(path.join(__dirname, '../public')));
  } else {
    app.use(express.static(path.join(__dirname, '../public')));
  }

  app.use(favicon(path.join(__dirname, '../public', 'favicon.ico')));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: false
  }));
  app.use(cookieParser());

  // i18n setup
  i18next
    .use(i18nBackend)
    .init({
      backend: {
        loadPath: 'resources/resources.{{lng}}.json'
      },
      fallbackLng: 'en'
    });

  app.use(morgan('dev'));
  app.use(i18nExpress.handle(i18next));

  app.locals = {
    baseUrl: config.baseUrlPath
  };
};
