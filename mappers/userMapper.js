'use strict';

module.exports = {
  buildPairingModel,
  mapUserModels
};

function buildPairingModel(users) {
  const responseModel = [];

  _shuffle(users);

  users.forEach((u, i) => {
    const targetIndex = ((i + 1) === users.length) ? 0 : (i + 1);
    responseModel.push({ users: [u, users[targetIndex]] });
  });

  return responseModel;
}

function mapUserModels(users) {
  return users.map((u) => ({
    displayName: [u.name.first, u.name.last].join(' '),
    email: u.email,
    phone: u.phone,
    userId: u.guid
  }));
}

function _shuffle(users) {
  for (let i = users.length - 1; i > 0; i--) {
    const x = Math.floor(Math.random() * (i + 1));
    [users[i], users[x]] = [users[x], users[i]];
  }
}
