'use strict';

/* eslint-disable no-console */
const config = require('config');
const express = require('express');
const app = express();
const middlewareConfig = require('./middleware/middlewareConfig.js');
const routeConfig = require('./routes/routeConfig');

require('dnscache')(config.dnsCache);

middlewareConfig(express, app);
routeConfig(app);

const server = app.listen(config.port, () => {
  console.log(`Listening on port ${config.port} ${Date()}`);
});

process.on('SIGTERM', () => {
  server.close(() => {
    console.log('Closing server connections');
    process.exit(0);
  });
});

process.on('uncaughtException', (err) => {
  console.log(`uncaughtException: ${err}`);
  console.log(err.stack);
  process.exit(1);
});
