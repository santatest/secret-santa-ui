'use strict';

const users = require('../data/users').users;

module.exports = {
  getUsers
};

function getUsers() {
  return users;
}
