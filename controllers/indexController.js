'use strict';

const userMapper = require('../mappers/userMapper');
const userService = require('../services/userService');

module.exports = {
  renderIndex: renderIndex
};

function renderIndex(req, res) {
  const users = userService.getUsers();
  const mappedUsers = userMapper.mapUserModels(users);
  const pairingViewModel = userMapper.buildPairingModel(mappedUsers);

  res.render('index', { pairings: pairingViewModel });
}
