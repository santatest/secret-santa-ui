'use strict';

const httpStatus = require('http-status');
const indexRoute = require('./index');

module.exports = function (app) {
  app.use('/healthcheck', (req, res) => {
    res.status(httpStatus.OK).send('I am healthy!');
  });

  app.use('/', indexRoute);

  // 404 Forward
  app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // Development Handler
  if (app.get('env') === 'loc' || app.get('env') === 'development') {
    app.use((err, req, res) => res.status(err.status || httpStatus.INTERNAL_SERVER_ERROR).send(err));
  }

  // Production Handler
  app.use((err, req, res) => {
    res.status(err.status || httpStatus.INTERNAL_SERVER_ERROR).send(err.message);
  });
};
