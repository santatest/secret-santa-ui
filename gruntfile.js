'use strict';

module.exports = function (grunt) {
  /* eslint-disable global-require, import/no-extraneous-dependencies */
  require('load-grunt-tasks')(grunt);

  const lintFiles = [
    'controllers/**/*.js',
    'middleware/**/*.js',
    'utils/**/*.js',
    'test/**/*.js',
    'delegates/**/*.js',
    'routes/**/*.js',
    'services/**/*.js',
    'mappers/**/*.js',
    'helpers/**/*.js',
    'clients/**/*.js',
    'clientWrappers/**/*.js',
    'repositories/**/*.js',
    './gruntfile.js',
    './app.js',
    'public/scripts/**/*.js'
  ];

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    eslint: {
      validate: {
        options: {
          fix: false
        },
        src: lintFiles
      },
      fix: {
        options: {
          fix: true
        },
        src: lintFiles
      }
    },

    nodemon: {
      script: 'app.js',
      options: {
        ext: 'js,html',
        ignore: ['node_modules/**', 'coverage/**', 'test/**', 'gruntfile.js'],
        env: { NODE_ENV: 'development' }
      }
    },

    concat: {
      jsBundle: {
        files: [
          {
            src: [
              'public/libs/jquery/dist/jquery.slim.js',
              'public/libs/bootstrap/dist/js/bootstrap.bundle.min.js'
            ],
            dest: 'public/assets/js/js-bundle.js'
          }
        ]
      },
      cssBundle: {
        files: [
          {
            src: [
              'public/libs/bootstrap/dist/css/bootstrap.min.css',
              'public/stylesheets/*.css',
              'public/libs/open-iconic/font/css/open-iconic-bootstrap.css',
              'public/libs/animate.css/animate.min.css'
            ],
            dest: 'public/assets/css/css-bundle.css'
          }
        ]
      }
    },

    uglify: {
      options: {
        // the banner is inserted at the top of the output
        banner: ''
      },
      dist: {
        files: {
          'public/assets/js/js-bundle.js': ['public/assets/js/js-bundle.js']
        }
      }
    },

    watch: {
      options: {
        livereload: true
      },
      scripts: {
        files: ['public/scripts/**/*.js', 'public/stylesheets/**/*.css'],
        tasks: ['concat']
      }
    }
  });

  grunt.registerTask('build', ['concat', 'uglify']);
  grunt.registerTask('default', ['concat', 'eslint:validate']);
  grunt.registerTask('start', ['nodemon']);
};
