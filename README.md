# README #

### Overview ###

This is a Node.js implementation of a secret santa application.

The app is deployed live via docker and kubernetes within Azure and can be reached at http://santa.seanenright.tech if needed.

### How do I get set up? ###

* Clone the repo.
* Install bower.
* Execute npm install in the working directory.
* Execute npm start to launch the app on port 3000.
* Launch in browser at http://localhost:3000 to view random pairings.


### Linting ###

* The app is linted using grunt and eslint.
* To validate execute: grunt eslint:validate.
* To fix execute: grunt eslint:fix.

### Unit Tests ###

The app is fully unit tested using the Mocha framework and nyc test coverage.

Execute npm test within the directory to run the unit tests and validate test coverage.

### Jenkins ###

Jenkins used for build/deploy.